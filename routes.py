from flask import Flask
from flask import render_template, url_for, redirect, make_response, request, jsonify, session
from exportify import app
from exportify import mongo
from exportify.model import User
from cerberus import Validator
from  exportify.validationschema import validate_signup,validate_signin
@app.route('/register',methods=["POST"])
def register():
	create_user = User()
	Getting_Postman_Data=request.json
	try:
		v = Validator(validate_signup)
		if v.validate(Getting_Postman_Data)==False:
			return jsonify({"error":v.errors})
		else:
			if create_user.check_for_email_exist(Getting_Postman_Data['_id']):
				return jsonify({'response':False},"User allready exist")
			else:
				create_user.insert(Getting_Postman_Data)
				return jsonify({"response": True},"User created successfully")
	except Exception as e:
		return jsonify({"response":False},str(e))   
@app.route("/login",methods=["POST"])
def login():
	login_user = User()
	Getting_Postman_Data=request.json
	try:
		v=Validator(validate_signin)
		if v.validate(Getting_Postman_Data)==False:
			return jsonify({"error":v.errors})
		else:
			if login_user.check_for_email(Getting_Postman_Data):
				if login_user.check_for_password(Getting_Postman_Data):
					#Here we created a jwt token and sent returned the response
					encode_jwt=jwt.encode({Emaiil_exist,secret,algorithm="HS256"}
						,{"exp": datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(hours=24)})
					return jsonify({"response": True},{"status":f'Acess Granted'}, {"Token": str(encode_jwt)})
				else:
					return jsonify({"response":False},'Wrong password')
			else:
				return jsonify({"response":False},"Wrong Email")
	except Exception as e:
		return jsonify({"response":False},str(e))
@app.route('/update',methods=['POST'])
def update():
	update_user=User()
	Getting_Postman_Data=request.json
	pass