validate_signup={
	'_id':{
	"type":"string",
	"required":True,
	"regex": "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$"
	},
	 "name":{
            "type":"string",
            "required":True,
            "empty":False
    },

    "age":{"type":"integer"
    },
    "mobile":{
              "type":"integer",
              "required":True
    }, 
    "password":{
                "type":"string",
                "required":True,
                "minlength":8,
                "maxlength":60
    },
    "company_name":{

                    "type":"string",
                    "required":True
    },
     "created_at":{

    }
}
validate_signin={
    "email_id":{
    "type":"string",
    "required":True,
    "regex": "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$"
    },
    "password":{
                "type":"string",
                "required":True,
                "minlength":8,
                "maxlength":60
    }
}